import { put, takeEvery, call } from 'redux-saga/effects';
import { setUsers } from '../store/userReducer';

const fetchUsersFromAPI = () =>
  fetch('https://jsonplaceholder.typicode.com/users?_limit=10');

function* fetchUserWorker() {
  const users = yield call(fetchUsersFromAPI);
  const json = yield call(() => new Promise((res) => res(users.json())));
  yield put(setUsers(json));
}

export function* userWatcher() {
  yield takeEvery('FETCH_USERS', fetchUserWorker);
}
