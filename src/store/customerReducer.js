const initialState = {
  customers: [],
};

export const customerReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_MANY_CUSTOMERS':
      return {
        ...state,
        customers: [...state.customers, ...action.payload],
      };
    case 'ADD_CUSTOMER':
      return {
        ...state,
        customers: [...state.customers, action.payload],
      };
    case 'REMOVE_CUSTOMER':
      return {
        ...state,
        customers: state.customers.filter(
          (customer) => customer.id !== action.payload
        ),
      };
    default:
      return state;
  }
};

export const addCustomer = (customer) => ({
  type: 'ADD_CUSTOMER',
  payload: customer,
});
export const removeCustomer = (id) => ({
  type: 'REMOVE_CUSTOMER',
  payload: id,
});

export const addManyCustomers = (payload) => ({
  type: 'ADD_MANY_CUSTOMERS',
  payload,
});
