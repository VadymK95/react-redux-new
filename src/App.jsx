import React from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import './App.css';
import { fetchCustomers } from './asyncActions/customers';
import { addCash, getCash } from './store/cashReducer';
import {
  asyncDecrementCreator,
  asyncIncrementCreator,
} from './store/countReducer';
import { addCustomer, removeCustomer } from './store/customerReducer';
import { fetchUsers } from './store/userReducer';

const App = () => {
  const dispatch = useDispatch();
  const cash = useSelector((state) => state.cash.cash);

  const users = useSelector((state) => state.users.users);
  const count = useSelector((state) => state.count.count);

  const customers = useSelector((state) => state.customers.customers);

  const onAddCash = (cash) => {
    dispatch(addCash(cash));
  };

  const onGetCash = (cash) => {
    dispatch(getCash(cash));
  };

  const onAddCustomer = (name) => {
    const customer = {
      name,
      id: Date.now().toString(),
    };
    dispatch(addCustomer(customer));
  };

  const onRemoveCustomer = (customer) => {
    dispatch(removeCustomer(customer.id));
  };

  return (
    <div className="app">
      <div style={{ fontSize: '2rem' }}>{cash}</div>
      <div>
        <button
          style={{ marginRight: '1rem' }}
          onClick={() => onAddCash(Number(prompt()))}
        >
          Add Cash
        </button>
        <button onClick={() => onGetCash(Number(prompt()))}>Get Cash</button>
        <button onClick={() => onAddCustomer(prompt())}>Add customer</button>
        <button onClick={() => dispatch(fetchCustomers())}>
          Show customers
        </button>
      </div>
      <div
        className="saga-block"
        style={{ border: '2px solid teal', marginTop: '10px', padding: '10px' }}
      >
        <div>{count}</div>
        <div>
          <button
            style={{ marginRight: '10px' }}
            onClick={() => dispatch(asyncIncrementCreator())}
          >
            INCREMENT
          </button>
          <button
            style={{ marginRight: '10px' }}
            onClick={() => dispatch(asyncDecrementCreator())}
          >
            DECREMENT
          </button>
          <button onClick={() => dispatch(fetchUsers())}>Get users</button>
        </div>
        <div className="saga-users">
          {users.map((user) => (
            <div className="saga-user">{user.name}</div>
          ))}
        </div>
      </div>
      {customers.length > 0 ? (
        <div style={{ textAlign: 'center', marginTop: '20px' }}>
          {customers.map((customer) => (
            <div onClick={() => onRemoveCustomer(customer)} key={customer.id}>
              {customer.name}
            </div>
          ))}
        </div>
      ) : (
        <div
          style={{ textAlign: 'center', fontSize: '2rem', marginTop: '20px' }}
        >
          The list is empty!
        </div>
      )}
    </div>
  );
};

export default App;
